<?php
/*
Plugin Name: Osinum WPGB
Plugin URI: 
Description: WP Grid Builder functions
Author: Bsa Web
Version: 1.0
Author URI:
Text Domain: osinum-wpgb
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! defined( 'OSINUM_WPGB_PATH' ) ) {
    define( 'OSINUM_WPGB_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'OSINUM_WPGB_URL' ) ) {
    define( 'OSINUM_WPGB_URL', plugin_dir_url( __FILE__ ) );
}

add_action( 'plugins_loaded', 'osinum_wpgb_init' );
function osinum_wpgb_init() {
    if ( function_exists( 'wp_grid_builder' ) ) {
        load_plugin_textdomain( 'osinum-wpgb', false, basename( dirname( __FILE__ ) ) . '/languages/' );
        require_once OSINUM_WPGB_PATH . '/inc/class-rich-button.php';
        require OSINUM_WPGB_PATH . '/inc/functions.php';
    }
}

add_filter( 'wp_grid_builder/frontend/register_scripts', 'osinum_wpgb_enqueue_front_script' );
function osinum_wpgb_enqueue_front_script( $scripts ) {
    $scripts[] = [
        'handle'    => 'osinum-wpgb',
        'source'    => OSINUM_WPGB_URL . 'assets/js/wpgb.js',
        'version'   => '1.0.2'
    ];
    return $scripts;
}


add_action( 'pre_get_posts', 'osinum_hide_private_tools', 20, 1 );

/**
 * Hide private tool except in the admin area.
 *
 * @param WP_Query $query
 * @return void
 */
function osinum_hide_private_tools( $query ) : void {

    if ( ! in_array( $query->get( 'post_type' ), [ 'tool' ] ) ) {
        return;
    }

    if ( ! is_admin() && is_post_type_archive( 'tool' ) && $query->is_main_query() ) {
        $query->set( 'meta_key', 'visibility' );
        $query->set( 'meta_value', 'public' );
    }
}
