// We listen every time a grid/template/content is initialized.
window.WP_Grid_Builder &&
	WP_Grid_Builder.on("init", function (wpgb) {
		// Render HTML in options select
		const observerSelects = new MutationObserver(function (mutations_list) {
			mutations_list.forEach(function (mutation) {
				mutation.addedNodes.forEach(function (added_node) {
					if (added_node.classList.contains("wpgb-select-dropdown")) {
						// get original select, options contains term icon & color added in PHP.
						const labelID = added_node
							.querySelector('ul[role="listbox"]')
							.getAttribute("aria-labelledby");
						const originalSelect = document
							.getElementById(labelID)
							.closest(".wpgb-select-facet")
							.querySelector(".wpgb-combobox");
						if (originalSelect) {
							added_node
								.querySelectorAll(".wpgb-select-item")
								.forEach(function (option) {
									const optionText = option.innerText;
									// get original option with icon & color
									const originalOption = [...originalSelect.options].find(
										(option) => option.text === optionText
									);
									if (originalOption) {
										const icon = originalOption.getAttribute("data-icon"),
											color = originalOption.getAttribute("data-color");
										if (icon && color) {
											// build option HTML by icon & color
											option.innerHTML =
												'<span class="wpgb-button-icon button theme-' +
												color +
												'"><svg aria-hidden="true" class="icon ' +
												icon +
												'"><use xlink:href="#' +
												icon +
												'"></use></svg></span>' +
												option.innerText;
										}
									}
								});
						}
					}
				});
			});
		});
		observerSelects.observe(document.body, { subtree: false, childList: true });

		// Show query results on facet changes
		wpgb.facets.on("change", function (slug, values) {
			if (values.length > 0) {
				document
					.querySelectorAll(".wpgb-active--hidden")
					.forEach(function (element) {
						element.classList.add("wpgb-hidden");
					});
				document
					.querySelectorAll(".wpgb-active--show")
					.forEach(function (element) {
						element.classList.remove("wpgb-hidden");
					});
				if (wpgb.element) {
					wpgb.element.scrollIntoView();
				}
			}
		});

		wpgb.facets.on("render", function (element, facet) {
			const swiper = element.querySelector(".swiper-element");
			if (swiper && !swiper.swiper && Swiper !== undefined) {
				let swiperElem = swiper.querySelector(".swiper");
				new Swiper(swiperElem, {
					observer: true,
					observeParents: true,
					observeSlideChildren: true,
					speed: 400,
					spaceBetween: 12,
					preventClicks: true,
					slideToClickedSlide: false,
					watchSlidesProgress: true,
					slidesPerView: swiperElem.getAttribute("data-slides-mobile")
						? swiperElem.getAttribute("data-slides-mobile")
						: 1,
					pagination: {
						el: swiper.querySelector(".swiper-pagination"),
						type: "bullets",
						clickable: true,
					},
					navigation: {
						nextEl: swiper.querySelector(".swiper-button-next"),
						prevEl: swiper.querySelector(".swiper-button-prev"),
						clickable: true,
					},
					breakpoints: {
						1024: {
							slidesPerView: swiperElem.getAttribute("data-slides")
								? swiperElem.getAttribute("data-slides")
								: 3,
							spaceBetween: parseFloat(swiperElem.getAttribute("data-space"))
								? parseFloat(swiperElem.getAttribute("data-space"))
								: 15,
						},
					},
				});
			}
		});

		const SHOW_ALL = document.querySelector(".wpgb-see-results");

		if (!SHOW_ALL) {
			return;
		}

        let activeHiddenResults = document.querySelectorAll(
            ".wpgb-active--show.wpgb-hidden"
        );

        let activeToHide = document.querySelector(
            ".archive__facets.wpgb-active--hidden"
        );

		SHOW_ALL.addEventListener("click", (e) => {
			e.preventDefault();
			const RESET = document.querySelector(".wpgb-button.wpgb-reset");

            if ( activeToHide && activeHiddenResults ) {
				activeToHide.classList.add( 'wpgb-hidden' );
                activeHiddenResults.forEach( ( result ) => {
					result.classList.remove("wpgb-hidden");
                } )
                activeToHide = null;
                activeHiddenResults = null;
            } else {
				if( RESET ) {
					RESET.parentNode.click();
                    RESET.click();
                }
            }

			const FIRST_GRID = document.querySelector(".wpgb-active--show");
			let scrollY = FIRST_GRID.getBoundingClientRect().top - 80;
			window.scrollTo({top: scrollY})
		});
	});
