<?php
/**
 * WPGB related functions
 *
 * @package osinum-wpgb
 */


 /**
  * Index two taxonomies in Thematiques select
  */
add_filter( 'wp_grid_builder/indexer/index_object', 'osinum_diag_filter_index', 10, 3 );
function osinum_diag_filter_index( $rows, $object_id, $facet ) {
    
    // We skip facet IDs different from 1.
    if ( 8 === $facet['id'] || 10 === $facet['id'] ) {
        $rows = [];
        // Create fake parents
        $rows[] = [
            'facet_name'    => __( 'situations topics', 'osinum-wpgb' ),
            'facet_value'   => 'situations-topics',
            'facet_id'      => 9999999,
            'facet_parent'  => 0
        ];
        $rows[] = [
            'facet_name'    => __( 'difficulties topics', 'osinum-wpgb' ),
            'facet_value'   => 'difficulties-topics',
            'facet_id'      => 8888888,
            'facet_parent'  => 0
        ];

        $topics = get_the_terms( $object_id, 'topic' );
        if ( $topics && ! is_wp_error( $topics ) ) {
            foreach( $topics as $topic ) {
                $rows[] = [
                    'facet_value' => $topic->slug,
                    'facet_name'  => $topic->name,
                    'facet_id'    => $topic->term_id,
                    'facet_parent'=> 9999999
                ];
            }
        }

        $difficulty_groups = get_the_terms( $object_id, 'difficulty_group' );
        if ( $difficulty_groups && ! is_wp_error( $difficulty_groups ) ) {
            foreach( $difficulty_groups as $difficulty_group ) {
                $rows[] = [
                    'facet_value' => $difficulty_group->slug,
                    'facet_name'  => $difficulty_group->name,
                    'facet_id'    => $difficulty_group->term_id,
                    'facet_parent'=> 8888888
                ];
            }
        }
    }

    if ( 11 === $facet['id'] ) {
        // display only popular tools
        $popular_tools = get_option( 'options_tool_populars' );
        if ( ! empty ( $popular_tools ) && ! empty( $rows ) ) {
            $tools_ids = array_column( $rows, 'facet_value' );
            $to_remove = array_diff( $tools_ids, $popular_tools );
            foreach( $rows as $key => $value ) {
                if (  in_array( $value[ 'facet_value' ], $to_remove ) ) {
                    unset( $rows[ $key ] );
                }
            }
        }
    }

    return $rows;
}

add_filter( 'wp_grid_builder/facet/selection', 'osinum_wpgb_facet_selection', 10, 3 );
/**
 * Add term meta to selected options
 */
function osinum_wpgb_facet_selection( $output, $item_slug, $item ) {
    
    $term = get_term( $item->facet_id );
    $post_type = get_post_type( $item->facet_id );

    $class_name = 'wpgb-button';
    $button_name = esc_html( $item->facet_name );

    if ( ! is_wp_error( $term ) && ! empty( $term ) ) {
        $term_icon = get_term_meta( $item->facet_id, 'icon', true );
        $term_color = get_term_meta( $item->facet_id, 'color', true );
        $class_name .= ' wpgb-button--' . $term->taxonomy;
        if ( $term_color ) {
            $class_name .= ' wpgb-button--has-color';
        }
        if ( $term_icon ) {
            $class_name .= ' wpgb-button--has-icon';
        }
        if ( $term_icon && $term_color ) {
            if ( $term->taxonomy === 'difficulty_group' ) { // difficulty group tax has dark icons
                $term_color .= '-dark';
            }
            $button_name = sprintf(
                '<span class="wpgb-button-icon button theme-%1$s"><svg aria-hidden="true" class="icon %2$s"><use xlink:href="#%2$s"></use></svg></span>%3$s',
                $term_color,
                $term_icon,
                $button_name
            );
        }
    }

    if ( ! is_wp_error( $post_type ) && ! empty( $post_type ) ) {
        $class_name .= ' wpgb-button--' . $post_type;
    }

    $input = sprintf(
        '<input type="hidden" name="%1$s" value="%2$s">',
        esc_attr( $item->facet_slug ),
        esc_attr( $item->facet_value )
    );

    $output  = '<div class="' . $class_name . '" role="button" aria-pressed="true" tabindex="0">';
        $output .= $input;
        $output .= '<span class="wpgb-button-control"></span>';
        $output .= '<span class="wpgb-button-label">' . $button_name . '</span>';
    $output .= '</div>';

	return $output;
}

add_filter( 'wp_grid_builder/facet/select', 'osinum_wpgb_render_facet_select', 10, 3 );
/**
 * Add term meta to select options
 */
function osinum_wpgb_render_facet_select( $output, $facet, $item ) {

    $is_term = get_term( $item->facet_id ) || $item->facet_id == 8888888 || $item->facet_id == 9999999;
    if ( ! $is_term ) {
        return $output;
    }

    $depth = 0;
    $selected = in_array( $item->facet_value, $facet['selected'], true );
	$disabled = ! $selected && empty( $item->count );
    $count = $facet['show_count'] ? '&nbsp;(' . (int) $item->count . ')' : '';

    // Disable fake parents added in 
    if ( $item->facet_id == 8888888 || $item->facet_id == 9999999 ) {
        $disabled = true;
        $count = '';
    }

    if ( $facet['hierarchical'] && $item->facet_parent > 0 ) {
        $depth++;
    }

    // Add term meta to option
    $term_icon = get_term_meta( $item->facet_id, 'icon', true );
    $term_color = get_term_meta( $item->facet_id, 'color', true );
    if ( get_term( $item->facet_id, 'difficulty_group' ) ) { // difficulty group tax has dark icons
        $term_color .= '-dark';
    }

    $output = sprintf(
        '<option value="%1$s"%2$s%3$s%4$s%5$s>%6$s%7$s%8$s</option>',
        esc_attr( $item->facet_value ),
        selected( $selected, true, false ),
        disabled( $disabled, true, false ),
        $term_icon ? ' data-icon="' . $term_icon . '"' : '',
        $term_color ? ' data-color="' . $term_color . '"' : '',
        str_repeat( '&emsp;', $depth ),
        esc_html( $item->facet_name ),
        $count
    );
    
    return $output;
}

add_filter( 'wp_grid_builder/facets', 'osinum_wpgb_register_custom_facets', 10, 1 );
function osinum_wpgb_register_custom_facets( $facets ) {
	$facets['rich_button'] = [
		'name'  => __( 'Rich buttons', 'osinum-wpgb' ),
		'type'  => 'filter',
		'class' => 'Rich_Button',
        'filter_type' => 'select'
	];
	return $facets;
}
