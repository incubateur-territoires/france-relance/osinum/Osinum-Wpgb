<?php
/**
 * Button facet
 *
 * @package   WP Grid Builder
 * @author    Loïc Blascos
 * @copyright 2019-2022 Loïc Blascos
 */

use WP_Grid_Builder\FrontEnd\Facets\Checkbox;
use WP_Grid_Builder\FrontEnd\Facets\Radio;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Button
 *
 * @class WP_Grid_Builder\FrontEnd\Facets\Button
 * @since 1.0.0
 */
class Rich_Button {

	/**
	 * Rendered items counter
	 *
	 * @since 1.0.0
	 * @var integer
	 */
	public $count = 0;

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Query facet choices
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param array $facet Holds facet settings.
	 * @return array Holds facet items.
	 */
	public function query_facet( $facet ) {
		if ( $facet['multiple'] ) {
			return ( new CheckBox() )->query_facet( $facet );
		}

		return ( new Radio() )->query_facet( $facet );

	}

	/**
	 * Render facet
	 *
	 * @since 1.2.0 Handle shortcode [number] in button label.
	 * @since 1.0.0
	 * @access public
	 *
	 * @param array $facet Holds facet settings.
	 * @param array $items Holds facet items.
	 * @return string Facet markup.
	 */
	public function render_facet( $facet, $items ) {

		$buttons = $this->render_buttons( $facet, $items );

		if ( empty( $buttons ) ) {
			return;
		}

		$output  = '<div class="wpgb-button-facet wpgb-buttons-rich-facet">';
        $output .= '<div class="swiper-element">';
        $output .= '<div class="swiper" data-slides="3.7" data-slides-mobile="1.5" data-space="12">';
		$output .= '<ul class="swiper-wrapper">';
		$output .= $buttons;
		$output .= '</ul>';
        $output .= '</div>';
        $output .= '<div class="swiper-button-prev"></div><div class="swiper-button-next"></div>';
        $output .= '</div>';

		if ( $this->count > $facet['display_limit'] ) {

			$output .= '<button type="button" class="wpgb-toggle-hidden" aria-expanded="false">';
			$output .= esc_html( str_replace( '[number]', $this->count - $facet['display_limit'], $facet['show_more_label'] ) );
			$output .= '</button>';

		}

		$output .= '</div>';

		return $output;

	}

	/**
	 * Render buttons
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param array $facet Holds facet settings.
	 * @param array $items Holds facet items.
	 * @return string Buttons markup.
	 */
	public function render_buttons( $facet, $items ) {

		$output = '';

		foreach ( $items as $index => $item ) {

			// Hide Children if allowed.
			if ( ! $facet['children'] && (int) $item->facet_parent > 0 ) {
				continue;
			}

			// Hide fake parents
			if ( $item->facet_id == 8888888 || $item->facet_id == 9999999 ) {
				continue;
			}

			// Hide empty item if allowed.
			if ( ! $facet['show_empty'] && ! $item->count ) {
				continue;
			}

			$hidden = $this->count >= $facet['display_limit'] ? ' hidden' : '';

			$output .= '<li' . esc_attr( $hidden ) . ' class="swiper-slide">';
			$output .= $this->render_button( $facet, $item );
			$output .= '</li>';

			// Count rendered items.
			++$this->count;

		}

		return $output;

	}

	/**
	 * Render button
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param array $facet Holds facet settings.
	 * @param array $item  Holds current list item.
	 * @return string Button markup.
	 */
	public function render_button( $facet, $item ) {

		$all_button = $facet['all_label'] && '' === $item->facet_value;

        $button_name = esc_html( $item->facet_name );

		// Select all button if no selection.
		if ( $all_button && empty( $facet['selected'] ) ) {
			$pressed = true;
		} else {
			$pressed = in_array( $item->facet_value, $facet['selected'], true );
		}

		$disabled    = ! $all_button && ! $pressed && empty( $item->count );
		$attributes  = $pressed ? ' aria-pressed="true"' : ' aria-pressed="false"';
		$attributes .= $disabled ? ' aria-disabled="true"' : '';
		$attributes .= $disabled ? ' tabindex="-1"' : ' tabindex="0"';

        $class_name = 'wpgb-button wpgb-button--rich';

		if ( isset( $item->facet_id ) ) {
            $term = get_term( $item->facet_id );
            $post_type = get_post_type( $item->facet_id );

            if ( term_exists( $term->term_id, $term->taxonomy ) ) {
				if ( ! is_wp_error( $term ) && ! empty( $term ) ) {
					$term_icon = get_term_meta( $item->facet_id, 'icon', true );
					$term_color = get_term_meta( $item->facet_id, 'color', true );
					$class_name .= ' wpgb-button--has-icon';
					$class_name .= ' wpgb-button--' . $term->taxonomy;

					if ( ! $term_icon && ! $term_color ) {
						$class_name .= ' wpgb-button--rich--default';
						$button_name = sprintf(
							'<div class="wpgb-rich-button button theme-%1$s theme-%1$s-hover"><div class="wpgb-rich-button__head"><span class="wpgb-button-icon"><svg aria-hidden="true" class="icon %2$s"><use xlink:href="#%2$s"></use></svg></span></div><div class="wpgb-rich-button__body"><span class="wpgb-rich-button__label">%3$s</span><span class="wpgb-rich-button__sublabel">%4$s</span></div></div>',
							'vert-1',
							'ecran',
							$button_name,
							get_taxonomy( $term->taxonomy )->labels->name
						);
					} else {
						$class_name .= ' wpgb-button--has-color';
						if ( $term->taxonomy === 'difficulty_group' ) { // difficulty group tax has dark icons
							$term_color .= '-dark';
						}
						$button_name = sprintf(
							'<div class="wpgb-rich-button"><div class="wpgb-rich-button__head"><span class="wpgb-button-icon button theme-%1$s theme-%1$s-hover"><svg aria-hidden="true" class="icon %2$s"><use xlink:href="#%2$s"></use></svg></span></div><div class="wpgb-rich-button__body"><span class="wpgb-rich-button__label">%3$s</span><span class="wpgb-rich-button__sublabel">%4$s</span></div></div>',
							$term_color,
							$term_icon,
							$button_name,
							get_taxonomy( $term->taxonomy )->labels->name
						);
					}
				}
            } else {
				if ( ! is_wp_error( $post_type ) && ! empty( $post_type ) ) {

					$tmp = get_post_type_object( $post_type )->label;
					if (get_post_type_object( $post_type )->label == 'Ressources') {
						$tmp = 'outils';
					}

					$class_name .= ' wpgb-button--' . $post_type;
					$button_name = sprintf(
						'<div class="wpgb-rich-button"><div class="wpgb-rich-button__head">%1$s</div><div class="wpgb-rich-button__body"><span class="wpgb-rich-button__label">%2$s</span><span class="wpgb-rich-button__sublabel">%3$s</span></div></div>',
						get_the_post_thumbnail( $item->facet_value, 'square' ),
						$button_name,
						$tmp
					);
				}
			}
        }

		$output  = '<div class="'. $class_name .'" role="button"' . $attributes . '>';
			$output .= $this->render_input( $facet, $item, $disabled );
			$output .= '<span class="wpgb-button-label">';
				$output .= $button_name;
			$output .= '</span>';
		$output .= '</div>';

		return apply_filters( 'wp_grid_builder/facet/button', $output, $facet, $item );

	}

	/**
	 * Render checkbox/radio input
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param array   $facet    Holds facet settings.
	 * @param array   $item     Holds current list item.
	 * @param boolean $disabled Input disabled state.
	 * @return string Checkbox/Radio input markup.
	 */
	public function render_input( $facet, $item, $disabled ) {

		return sprintf(
			'<input type="hidden" name="%1$s" value="%2$s"%3$s>',
			esc_attr( $facet['slug'] ),
			esc_attr( $item->facet_value ),
			disabled( $disabled, true, false )
		);

	}

	/**
	 * Query object ids (post, user, term) for selected facet values
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param array $facet Holds facet settings.
	 * @return array Holds queried facet object ids.
	 */
	public function query_objects( $facet ) {
		if ( $facet['multiple'] ) {
			$instance = new CheckBox();
		} else {
			$instance = new Radio();
		}
		return $instance->query_objects( $facet );

	}
}
